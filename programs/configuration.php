<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/functions.php';




class libpaymentsips_Configuration {

	/**
	 * @var string Name of this configuration.
	 */
	public $name;

	/**
	 * @var string Optional description of this configuration.
	 */
	public $description;

	/**
	 * @var string	Path to executable used for encoding a request to the payment server.
	 */
	public $requestFile;

	/**
	 * @var string	Path to executable used for decoding a response from the payment server.
	 */
	public $responseFile;

	/**
	 * @var string	Path to pathfile, the main SIPS api configuration file.
	 */
	public $pathFile;

	/**
	 * @var string	$merchantId The merchant id is provided by (marcanet, sogenactif...) when the
	 * 						    shop is registered. It is usually the SIRET number prefixed by 0.
	 */
	public $merchantId;

	/**
	 * @var string	$merchantCountry Country code for the merchant.
	 * @see libpaymentsips_getAvailableCountries()
	 */
	public $merchantCountry;

}



/**
 *
 * @return bab_registry
 */
function libpaymentsips_getRegistry()
{
	$registry = bab_getRegistryInstance();

	$registry->changeDirectory('/LibPaymentSips');

	return $registry;
}




function libpaymentsips_getConfigurationNames()
{
	$registry = libpaymentsips_getRegistry();

	$registry->changeDirectory('configurations');

	$names = array();

	while ($name = $registry->fetchChildDir()) {
		$name = substr($name, 0, -1);
		$names[$name] = $name;
	}

	return $names;
}




/**
 * Returns all configuration information in a libpaymentsips_Configuration object.
 *
 * @param string $name		The configuration name.
 *
 * @return libpaymentsips_Configuration
 */
function libpaymentsips_getConfiguration($name = null)
{
	$registry = libpaymentsips_getRegistry();

	if (!isset($name)) {
		$name = libpaymentsips_getDefaultConfigurationName();
	}

	$registry->changeDirectory('configurations');

	$registry->changeDirectory($name);

	$configuration = new libpaymentsips_Configuration();

	while ($key = $registry->fetchChildKey()) {
		$configuration->$key = $registry->getValue($key);
	}

	return $configuration;
}




/**
 * Sets the default configuration name.
 *
 * @param string $name		The configuration name.
 *
 * @return void
 */
function libpaymentsips_setDefaultConfigurationName($name)
{
	$registry = libpaymentsips_getRegistry();
	$identifier = $registry->setKeyValue('default', $name);
}





/**
 * Returns the default configuration name.
 *
 * @return void
 */
function libpaymentsips_getDefaultConfigurationName()
{
	$registry = libpaymentsips_getRegistry();
	return $registry->getValue('default');
}





/**
 * Renames a configuration.
 *
 * @param string $originalName
 * @param string $newName
 *
 * @return bool
 */
function libpaymentsips_renameConfiguration($originalName, $newName)
{
	$registry = libpaymentsips_getRegistry();

	$registry->changeDirectory('configurations');

	return $registry->moveDirectory($originalName, $newName);
}





/**
 * Sets all configuration information.
 *
 * @param libpaymentsips_Configuration $configuration
 *
 * @return void
 */
function libpaymentsips_saveConfiguration($name, libpaymentsips_Configuration $configuration)
{
	$registry = libpaymentsips_getRegistry();

	$registry->changeDirectory('configurations');

	$registry->changeDirectory($name);

	$registry->setKeyValue('name', $configuration->name);
	$registry->setKeyValue('description', $configuration->description);
	$registry->setKeyValue('requestFile', $configuration->requestFile);
	$registry->setKeyValue('responseFile', $configuration->responseFile);
	$registry->setKeyValue('pathFile', $configuration->pathFile);
	$registry->setKeyValue('merchantId', $configuration->merchantId);
	$registry->setKeyValue('merchantCountry', $configuration->merchantCountry);
}





/**
 * Deletes the specified configuration.
 */
function libpaymentsips_deleteConfiguration($name)
{
	$registry = libpaymentsips_getRegistry();

	$registry->changeDirectory('configurations');

	if (!$registry->isDirectory($name)) {
		return false;
	}

	$registry->changeDirectory($name);

	$registry->deleteDirectory();

	return true;
}

