<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */


require_once dirname(__FILE__) . '/functions.php';
require_once dirname(__FILE__) . '/configuration.php';


/* @var $I Func_Icons */
$I = bab_functionality::get('Icons');
$I->includeCss();


/**
 *
 * @param string $labelText
 * @param Widget_Item $item
 * @return Widget_VBoxLayout
 */
function libpaymentsips_labelledField($labelText, Widget_Item $item)
{
	$W = libpaymentsips_Widgets();
	$label = $W->Label($labelText);

	$label->setAssociatedWidget($item);
	$label->colon();

	return $W->VBoxItems(
		$label,
		$item
	)->setVerticalSpacing(4, 'px');
}


/**
 *
 * @return Widget_Frame
 */
function libpaymentsips_TpeEditor()
{
	$W = libpaymentsips_Widgets();
	$editor = $W->Frame();

	$countrySelect = $W->Select();

	$countrySelect->addOption('', '');
	$availableCountries = libpaymentsips_getAvailableCountries();
	foreach ($availableCountries as $countryCode => $countryName) {
		$countrySelect->addOption($countryCode, $countryName);
	}

	$layout = $W->VBoxItems(

		libpaymentsips_labelledField(
			libpaymentsips_translate('Name'),
			$W->LineEdit()
				->setMandatory(true, libpaymentsips_translate('You must specify a unique name for this TPE.'))
				->addClass('widget-fullwidth')
				->setName('name')
		),
		libpaymentsips_labelledField(
			libpaymentsips_translate('Description'),
			$W->TextEdit()
				->setLines(1)
				->addClass('widget-fullwidth')
				->setName('description')
		),
		libpaymentsips_labelledField(
			libpaymentsips_translate('Merchant ID'),
			$W->LineEdit()
				->addClass('widget-fullwidth')
				->setName('merchantId')
		),
		libpaymentsips_labelledField(
			libpaymentsips_translate('Path to \'pathfile\''),
			$W->LineEdit()
				->addClass('widget-fullwidth')
				->setName('pathFile')
		),
		libpaymentsips_labelledField(
			libpaymentsips_translate('Path to \'request\' binary file'),
			$W->LineEdit()
				->addClass('widget-fullwidth')
				->setName('requestFile')
		),
		libpaymentsips_labelledField(
			libpaymentsips_translate('Path to \'response\' binary file'),
			$W->LineEdit()
				->addClass('widget-fullwidth')
				->setName('responseFile')
		),
		libpaymentsips_labelledField(
			libpaymentsips_translate('Merchant country'),
			$countrySelect
				->setName('merchantCountry')
		)

	)->setVerticalSpacing(1, 'em');

	$editor->setLayout($layout);

	return $editor;
}



/**
 *
 * @param string $tpe
 */
function libpaymentsips_editTpe($tpe = null)
{
	$W = libpaymentsips_Widgets();

	$addon = bab_getAddonInfosInstance('LibPaymentSips');
	$addonUrl = $addon->getUrl();


	$page = $W->babPage();
	$page->setLayout($W->VBoxLayout()->setVerticalSpacing(2, 'em'));



	$form = $W->Form();
	$form->addClass('BabLoginMenuBackground');
	$form->setLayout($W->VBoxLayout()->setVerticalSpacing(2, 'em'));
	$form->setName('tpe');

	$editor = libpaymentsips_TpeEditor();

	$form->addItem($editor);

	$form->addItem(
		$W->FlowItems(
			$W->SubmitButton()
				->setLabel(libpaymentsips_translate('Save configuration'))
				->addClass('icon ' . Func_Icons::ACTIONS_DIALOG_OK),
			$W->Link(
				libpaymentsips_translate('Cancel'),
				$addonUrl . 'systemconf&idx=displayTpeList'
			)->addClass('icon ' . Func_Icons::ACTIONS_DIALOG_CANCEL)
		)
		->setSpacing(1, 'em')
		->addClass(Func_Icons::ICON_LEFT_16)
	);

	$form->setHiddenValue('tg', bab_rp('tg'));
	$form->setHiddenValue('idx', 'saveTpe');

	if (isset($tpe)) {
		$title = libpaymentsips_translate('Edit TPE configuration');

		$configuration = libpaymentsips_getConfiguration($tpe);
		$form->setValues(
			array(
				'tpe' => array(
					'name' => $configuration->name,
					'description' => $configuration->description,
					'requestFile' => $configuration->requestFile,
					'responseFile' => $configuration->responseFile,
					'pathFile' => $configuration->pathFile,
					'merchantId' => $configuration->merchantId,
					'merchantCountry' => $configuration->merchantCountry,
				)
			)
		);
		$form->setHiddenValue('tpe[originalName]', $tpe);
	} else {
		$title = libpaymentsips_translate('New TPE configuration');
	}

	$page->addItem($W->Title($title, 1)->addClass('title'));

	$page->addItem($form);

	$page->displayHtml();
}




function libpaymentsips_saveTpe($tpe)
{
	$configuration = new libpaymentsips_Configuration();

	$configuration->name = $tpe['name'];
	$configuration->description = $tpe['description'];
	$configuration->requestFile = $tpe['requestFile'];
	$configuration->responseFile = $tpe['responseFile'];
	$configuration->pathFile = $tpe['pathFile'];
	$configuration->merchantId = $tpe['merchantId'];
	$configuration->merchantCountry = $tpe['merchantCountry'];


	if (isset($tpe['originalName']) && $tpe['name'] != $tpe['originalName']) {
		libpaymentsips_renameConfiguration($tpe['originalName'], $tpe['name']);
	}

	libpaymentsips_saveConfiguration($tpe['name'], $configuration);
}




function libpaymentsips_displayTpeList()
{
	$W = libpaymentsips_Widgets();

	$page = $W->babPage();
	$page->setLayout($W->VBoxLayout()->setVerticalSpacing(2, 'em'));

	$page->addItem(
		$W->Title(libpaymentsips_translate('List of configured TPE'), 1)
			->addClass('title')
	);

	$tpeNames = libpaymentsips_getConfigurationNames();


	$tableView = $W->TableView();


	$addon = bab_getAddonInfosInstance('LibPaymentSips');
	$addonUrl = $addon->getUrl();

	$tableView->addItem(
		$W->Label(libpaymentsips_translate('Name')),
		0, 0
	);
	$tableView->addItem(
		$W->Label(libpaymentsips_translate('Pathfile')),
		0, 1
	);
	$tableView->addItem(
		$W->Label(libpaymentsips_translate('Merchant ID')),
		0, 2
	);
	$tableView->addItem(
		$W->Label(libpaymentsips_translate('Merchant country')),
		0, 3
	);

	$tableView->addItem(
		$W->Label(''),
		0, 4
	);

	$tableView->addColumnClass(4, 'widget-column-thin');

	$tableView->addSection('tpe');
	$tableView->setCurrentSection('tpe');


	$defaultName = libpaymentsips_getDefaultConfigurationName();

	$row = 0;
	foreach ($tpeNames as $name) {

		$configuration = libpaymentsips_getConfiguration($name);

		$nameLabel = $W->Label($configuration->name);
		if ($name == $defaultName) {
			$nameLabel->addClass('icon ' . Func_Icons::ACTIONS_DIALOG_OK);
		}

		$tableView->addItem(
			$W->VBoxItems(
				$nameLabel,
				$W->Label($configuration->description)->addClass('widget-small')
			)->addClass(Func_Icons::ICON_LEFT_16),
			$row, 0
		);
		$tableView->addItem(
			$W->Label($configuration->pathFile),
			$row, 1
		);
		$tableView->addItem(
			$W->Label($configuration->merchantId),
			$row, 2
		);

		$tableView->addItem(
			$W->Label($configuration->merchantCountry),
			$row, 3
		);

		$tableView->addItem(
			$W->FlowItems(
				$W->Link(
					libpaymentsips_translate('Edit'),
					$addonUrl . 'systemconf&idx=editTpe&tpe=' . $name
				)->addClass('icon widget-nowrap ' . Func_Icons::ACTIONS_DOCUMENT_EDIT),

				$W->Link(
					libpaymentsips_translate('Set default'),
					$addonUrl . 'systemconf&idx=setDefaultTpe&tpe=' . $name
				)->addClass('icon widget-nowrap ' . Func_Icons::ACTIONS_DIALOG_OK),

				$W->Link(
						libpaymentsips_translate('Delete'),
						$addonUrl . 'systemconf&idx=deleteTpe&tpe=' . $name
				)->setConfirmationMessage(sprintf(libpaymentsips_translate('Are you sure you want to delete the TPE \'%s\'?'), $name))
				->addClass('icon widget-nowrap ' . Func_Icons::ACTIONS_EDIT_DELETE)
			)
			->setSpacing(4, 'px')
			->addClass(Func_Icons::ICON_LEFT_16),
			$row, 4
		);

		$row++;
	}

	$page->addItem($tableView);

	$page->addItem(
		$W->FlowItems(
			$W->Link(
				libpaymentsips_translate('Add configuration'),
				$addonUrl . 'systemconf&idx=editTpe'
			)->addClass('icon ' . Func_Icons::ACTIONS_LIST_ADD)
		)->addClass(Func_Icons::ICON_LEFT_16)
	);

	$page->displayHtml();
}




function libpaymentsips_deleteTpe($tpe)
{
	libpaymentsips_deleteConfiguration($tpe);
}



function libpaymentsips_setDefaultTpe($tpe)
{
	libpaymentsips_setDefaultConfigurationName($tpe);
}


/* main */

if (!bab_isUserAdministrator())
{
	return;
}



$idx= bab_rp('idx', 'displayTpeList');

$addon = bab_getAddonInfosInstance('LibPaymentSips');

switch ($idx)
{
	case 'displayTpeList':
		$babBody->addItemMenu('list', libpaymentsips_translate('List'), $GLOBALS['babAddonUrl'] . 'systemconf&idx=displayTpeList');
		libpaymentsips_displayTpeList();
		break;

	case 'editTpe':
		$tpe = bab_rp('tpe', null);
		$editor = libpaymentsips_editTpe($tpe);
		break;

	case 'saveTpe':
		$tpe = bab_rp('tpe', null);
		$editor = libpaymentsips_saveTpe($tpe);
		header('location:'. $addon->getUrl() .'systemconf&idx=displayTpeList');
		die;

	case 'deleteTpe':
		$tpe = bab_rp('tpe', null);
		libpaymentsips_deleteTpe($tpe);
		header('location:'. $addon->getUrl() .'systemconf&idx=displayTpeList');
		die;

	case 'setDefaultTpe':
		$tpe = bab_rp('tpe', null);
		libpaymentsips_setDefaultTpe($tpe);
		header('location:'. $addon->getUrl() .'systemconf&idx=displayTpeList');
		die;
}

