<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/functions.php';





/**
 * The data returned by payment server.
 * @var array $data
 */
$data = bab_rp('DATA');

/* @var $Sips Func_Payment_Sips */
$Sips = bab_Functionality::get('Payment/Sips');


try {

	$result = $Sips->checkResponse($data);

} catch (libpayment_AuthorisationException $e) {
	
	// erreur lors de l'autorisation sur la passerelle de paiement, ne pas notifier l'application car cela est fait par autoresponse
	bab_debug($e->getMessage());
	return;
	
} catch (libpayment_Exception $e) {
	
	// erreur interne, la passerelle de paiement n'a pas pu etre appellee

	bab_debug($e->getMessage()."\n".$e->getCommandLine()."\n".$e->getGatewayMessage());
	return;
}

// There was no exception thrown by checkResponse(), the payment was successful.

//The payment token is stored in the 'caddie' parameter.
$token = $result['caddie'];



$paymentLogSet = new payment_logSet();

$paymentLog = $paymentLogSet->get($paymentLogSet->token->is($token));

if (!$paymentLog) {
	bab_debug(sprintf('Received payment response for non existing payment token (%s)', $token));
	return;
}


$payment = unserialize($paymentLog->payment);

// bab_debug($payment);

$paymentEvent = $Sips->newEventPaymentUserReturn();

$paymentEvent->setPayment($payment);
$paymentEvent->setResponseAmount($result['amount'] / 100);
$paymentEvent->setResponseAuthorization($result['authorisation_id']);
$paymentEvent->setResponseTransaction($result['transaction_id']);


bab_fireEvent($paymentEvent);

die();
