<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */



function LibPaymentSips_onDeleteAddon()
{


	$addon = bab_getAddonInfosInstance('LibPaymentSips');
	$addon->unregisterFunctionality('Payment/Sips');

	return true;
}



function LibPaymentSips_upgrade($sVersionBase, $sVersionIni)
{
	global $babDB;
	require_once $GLOBALS['babInstallPath'] . 'utilit/path.class.php';
	require_once $GLOBALS['babInstallPath'] . 'utilit/devtools.php';


	$Payment = bab_Functionality::get('Payment');
	if (!$Payment) {

		return false;
	}

	
	$addon = bab_getAddonInfosInstance('LibPaymentSips');
	$addon->registerFunctionality('Payment/Sips', 'paymentsips.func.php');

	$srcpath = new bab_Path(bab_getAddonInfosInstance('LibPaymentSips')->getImagesPath(), 'logo');
	$destpath = new bab_Path('images/addons/LibPaymentSips/logo');
	$destpath->createDir();
	foreach ($srcpath as $file) {
		/*@var $file bab_Path */

		$newfile = clone $destpath;
		$newfile->push($file->getBasename());
		if (!$file->isDir()) {
			copy($file->toString(), $newfile->toString());
		}
	}

	return true;

}
