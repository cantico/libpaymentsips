<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/functions.php';

bab_functionality::includeFile('Payment');

class libpayment_Payment_Sips extends libpayment_Payment
{


}


class Func_Payment_Sips extends Func_Payment
{

	private $currencies = array(
	    'EUR' => '978',
	    'USD' => '840',
	    'CHF' => '756',
	    'GBP' => '826',
	    'CAD' => '124',
	    'JPY' => '392',
	    'MXP' => '484',
	    'TRL' => '792',
	    'AUD' => '036',
	    'NZD' => '554',
	    'NOK' => '578',
	    'BRC' => '986',
	    'ARP' => '032',
	    'KHR' => '116',
	    'TWD' => '901',
	    'SEK' => '752',
	    'DKK' => '208',
	    'KRW' => '410',
	    'SGD' => '702'
    );

	private function getCurrentLang()
	{
		switch ($GLOBALS['babLanguage']) {
			case 'fr':
				return 'fr';

			case 'en':
				return 'en';

			case 'es':
				return 'sp';

			case 'de':
				return 'ge';
		}

		return '';
	}

	/**
	 * @param string $currency A libpayment_Payment::CURRENCY_xxx code.
	 */
	private function getCurrencyCode($currency)
	{
		if (isset($this->currencies[$currency])) {
			return $this->currencies[$currency];
		}

		return '';
	}

	/**
	 * Returns the command line used to make a payment server request.
	 *
	 * @param  libpayment_Payment $payment
	 * @return string
	 */
	protected function getRequestCommandLine(libpayment_Payment $payment)
	{
		global $babUrl;

		$addon = bab_getAddonInfosInstance('LibPaymentSips');
		require_once dirname(__FILE__) . '/configuration.php';

		$configuration = libpaymentsips_getConfiguration();

		$commandLine = $configuration->requestFile;

		$parameters = array(
			'merchant_id' => $configuration->merchantId,
			'merchant_country' => $configuration->merchantCountry,
			'pathfile' => $configuration->pathFile,
			'amount' => round($payment->getAmount(), 2) * 100,
			'currency_code' => $this->getCurrencyCode($payment->getCurrency()),
			'cancel_return_url' => $babUrl, // libpayment_EventPaymentCancel not used on SIPS because we get back to homepage
			'normal_return_url' => $addon->getUrl() . 'userreturn', 
			'automatic_response_url' => $addon->getUrl() . 'autoresponse',
			'language' => $this->getCurrentLang(),
			'caddie' => $payment->getToken()
		);


		foreach ($parameters as $parameterName => $parameterValue) {
			$commandLine .= ' ' . escapeshellarg($parameterName . '=' . $parameterValue);
		}


		return $commandLine;
	}







	/**
	 * Returns the command line used to parse a payment server response.
	 *
	 * @param  string $responseData
	 * @return string
	 */
	protected function getResponseCommandLine($responseData)
	{

		$configuration = libpaymentsips_getConfiguration();

		$commandLine = $configuration->responseFile;

		$parameters = array(
			'pathfile' => $configuration->pathFile,
			'message' => $responseData
		);


		foreach ($parameters as $parameterName => $parameterValue) {
			$commandLine .= ' ' . escapeshellarg($parameterName . '=' . $parameterValue);
		}

		return $commandLine;
	}






	/**
	 * Returns the html code to display to allow the user to perform the payment with the payment server.
	 *
	 * @param libpayment_Payment $payment The payment object containing all relevant information about the payment to perform.
	 *
	 * @return string	The HTML code to display that will allow the user to perform the payment.
	 *
	 * @throws libpayment_Exception
	 */
	public function getPaymentRequestHtml(libpayment_Payment $payment)
	{
		$commandLine = $this->getRequestCommandLine($payment);

		bab_debug($commandLine);

		$result = exec($commandLine);

		bab_debug($result);

		//	Result of command line : $result=!code!error!buffer!
		//	    - code=0	: the function returns an hhtml page in the 'buffer' variable
		//	    - code=-1 	: the function returns an error message int the 'error' variable

		$tableau = explode ('!', $result);

		$code = isset($tableau[1]) ? $tableau[1] : null;
		$error = isset($tableau[2]) ? $tableau[2] : null;
		$message = isset($tableau[3]) ? $tableau[3] : null;

		bab_debug($error);

		if ($code !== '0') {

			if (empty($code)) {
				$exception = new libpayment_Exception(libpayment_translate("Erreur appel request, executable request non trouve"));
				$exception->setCommandLine($commandLine);
				throw $exception;
			} else {
				$exception = new libpayment_Exception(libpayment_translate("Erreur appel API de paiement"));
				$exception->setCommandLine($commandLine);
				$exception->setGatewayMessage($error);
				throw $exception;
			}

			return false;
		}

		$this->logPayment($payment);
		return $message;

	}



	/**
	 *
	 * @param array $data
	 *
	 * @return array
	 */
	public function checkResponse($data)
	{
		require_once dirname(__FILE__) . '/configuration.php';

		$configuration = libpaymentsips_getConfiguration();

		$commandLine = $this->getResponseCommandLine($data);


		// Appel du binaire response
		$result = exec($commandLine);
		$tableau = explode ('!', $result);
		$code = $tableau[1];
		$error = $tableau[2];

		$result = array();

		if (($code == '') && ($error == '')) {
			$exception = new libpayment_Exception(libpayment_translate("Erreur appel response, executable response non trouve"));
			$exception->setCommandLine($commandLine);
			throw $exception;
			return false;
			
		} elseif ($code != 0) {
			
			$exception = new libpayment_Exception(libpayment_translate("Erreur appel API de paiement"));
			$exception->setGatewayMessage($error);
			$exception->setCommandLine($commandLine);
			throw $exception;
			return false;
		}

		$result['code'] 				= $tableau[1];
		$result['error'] 				= $tableau[2];
		$result['merchant_id'] 			= $tableau[3];
		$result['merchant_country'] 	= $tableau[4];
		$result['amount'] 				= $tableau[5];
		$result['transaction_id'] 		= $tableau[6];
		$result['payment_means'] 		= $tableau[7];
		$result['transmission_date'] 	= $tableau[8];
		$result['payment_time'] 		= $tableau[9];
		$result['payment_date'] 		= $tableau[10];
		$result['response_code'] 		= $tableau[11];
		$result['payment_certificate'] 	= $tableau[12];
		$result['authorisation_id'] 	= $tableau[13];
		$result['currency_code'] 		= $tableau[14];
		$result['card_number'] 			= $tableau[15];
		$result['cvv_flag'] 			= $tableau[16];
		$result['cvv_response_code'] 	= $tableau[17];
		$result['bank_response_code'] 	= $tableau[18];
		$result['complementary_code'] 	= $tableau[19];
		$result['complementary_info'] 	= $tableau[20];
		$result['return_context'] 		= $tableau[21];
		$result['caddie'] 				= $tableau[22];
		$result['receipt_complement'] 	= $tableau[23];
		$result['merchant_language'] 	= $tableau[24];
		$result['language'] 			= $tableau[25];
		$result['customer_id'] 			= $tableau[26];
		$result['order_id'] 			= $tableau[27];
		$result['customer_email'] 		= $tableau[28];
		$result['customer_ip_address'] 	= $tableau[29];
		$result['capture_day'] 			= $tableau[30];
		$result['capture_mode'] 		= $tableau[31];
		$result['data'] 				= $tableau[32];
		
		// check response code
		
		if ('00' !== $result['response_code'])
		{
			switch($result['response_code'])
			{
				case '02':
					$message = libpaymentsips_translate('Authorization request by phone');
					break;
					
				case '03':
					$message = libpaymentsips_translate('Invalid merchant_id field');
					break;
					
				case '05':
					$message = libpaymentsips_translate('Authorization denied');
					break;
					
				case '12':
					$message = libpaymentsips_translate('Invalid transaction');
					break;
					
				case '17':
					$message = libpaymentsips_translate('Canceled');
					break;
					
				case '30':
					$message = libpaymentsips_translate('Format error');
					break;
					
				case '34';
					$message = libpaymentsips_translate('Suspected fraud');
					break;
					
				case '75':
					$message = libpaymentsips_translate('Number of attempts to enter the card number exceeded.');
					break;
					
				case '90':
					$message = libpaymentsips_translate('Service temporarily unavailable');
					break;
					
				default:
					$message = libpaymentsips_translate('Unknown error code');
					break;
			}
			
			
			$exception = new libpayment_AuthorisationException($message);
			$exception->setCommandLine($commandLine);
			
			if (!$exception->initPaymentFromToken($result['caddie']))
			{
				throw new libpayment_Exception(sprintf('Received payment response for non existing payment token (%s)', $result['caddie']));
			}
			
			$exception->payment_means = $result['payment_means'];
			$exception->response_code = $result['response_code'];
			$exception->bank_response_code = $result['bank_response_code'];
			$exception->card_number = $result['card_number'];
			
			if (preg_match('/(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})/', $result['transmission_date'], $m))
			{
				list(, $y, $m, $d, $h, $i, $s) = $m;
				
				// GMT date, transformer en heure locale
				
				require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
				
				$transmission_date = new BAB_DateTime($y, $m, $d, $h, $i, $s);
				$transmission_date->add($transmission_date->getTimeZoneOffset('UTC'), BAB_DATETIME_SECOND);
				
				$exception->transmission_date = $transmission_date;
			}
			
			throw $exception;
		}
		

		return $result;
	}





	public function getTpeList()
	{
		$tpeNames = libpaymentsips_getConfigurationNames();

		return $tpeNames;
	}


	public function getDescription()
	{
		return libpayment_translate("Merc@net Payment Gateway");
	}
}


