; <?php/*
[general]
name                          = "LibPaymentSips"
version                       = "0.2.2"
addon_type                    = "LIBRARY"
mysql_character_set_database  = "latin1,utf8"
encoding                      = "UTF-8"
description                   = "Sips Payment Gateway functionality"
description.fr                = "Librairie partagée de passerelle de paiement vers Sips"
delete                        = "1"
longdesc                      = ""
ov_version                    = "8.1.98"
php_version                   = "5.2.0"
addon_access_control          = "0"
configuration_page            = "systemconf"
db_prefix                     = "libpaymentsips"
author                        = "Cantico ( support@cantico.fr )"
icon                          = "credit-card.png"
tags						  = "library,payment"

[addons]
widgets						  = "1.0.10"
LibPayment					  = "0.2"

;*/ ?>